-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2014 at 09:25 PM
-- Server version: 5.5.28
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `universaldb`
--
CREATE DATABASE IF NOT EXISTS `realestateDB` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `realestateDB`;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `Addr_AddressID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Addr_EntityID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_EntityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_AddressStreet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_POBox` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_District` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_CareOf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Addr_AddressID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`Addr_AddressID`, `Addr_EntityID`, `Addr_EntityType`, `Addr_AddressStreet`, `Addr_POBox`, `Addr_Town`, `Addr_Region`, `Addr_District`, `Addr_Country`, `Addr_CareOf`, `Addr_Deleted`, `created_at`, `updated_at`) VALUES
(1, '2', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:03:26', '2014-08-22 01:03:26'),
(2, '3', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:06:45', '2014-08-22 01:06:45'),
(3, '4', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(4, '9', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:46:37', '2014-08-22 01:46:37'),
(5, '10', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:48:55', '2014-08-22 01:48:55'),
(6, '11', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:57:52', '2014-08-22 01:57:52'),
(7, '12', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:58:20', '2014-08-22 01:58:20'),
(8, '13', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:58:49', '2014-08-22 01:58:49'),
(9, '14', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:09:47', '2014-08-22 02:09:47'),
(10, '15', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(11, '16', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(12, '17', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(13, '18', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(14, '19', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(15, '20', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(16, '21', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:22:45', '2014-08-22 02:22:45'),
(17, '22', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(18, '23', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(19, '24', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(20, '25', 'Person', 'asdfasdfa', NULL, 'asdfasdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(21, '26', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:46:59', '2014-08-22 02:46:59'),
(22, '27', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 15:39:11', '2014-08-22 15:39:11'),
(23, '28', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 15:40:00', '2014-08-22 15:40:00'),
(24, '29', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 15:40:44', '2014-08-22 15:40:44'),
(25, '30', 'Person', 'street', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 16:37:07', '2014-08-22 16:37:07'),
(26, '31', 'Person', 'street', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(27, '32', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(28, '33', 'Person', 'dr. cessay', NULL, 'kololi', NULL, 'Serre Kunda West', NULL, NULL, 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE IF NOT EXISTS `agents` (
  `agen_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agen_persID` int(11) NOT NULL DEFAULT '0',
  `agen_contID` int(11) NOT NULL DEFAULT '0',
  `agen_addrID` int(11) NOT NULL DEFAULT '0',
  `agen_prospID` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`agen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`agen_id`, `agen_persID`, `agen_contID`, `agen_addrID`, `agen_prospID`, `created_at`, `updated_at`) VALUES
(1, 24, 35, 19, 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(2, 25, 38, 20, 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `agent_prospectives`
--

CREATE TABLE IF NOT EXISTS `agent_prospectives` (
  `apro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apro_persID` int(11) NOT NULL DEFAULT '0',
  `apro_agenID` int(11) NOT NULL DEFAULT '0',
  `apro_addrID` int(11) NOT NULL DEFAULT '0',
  `apro_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apro_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apro_remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apro_deleted` int(11) NOT NULL DEFAULT '0',
  `apro_visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`apro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `calender_table`
--

CREATE TABLE IF NOT EXISTS `calender_table` (
  `caln_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caln_rentID` int(11) NOT NULL DEFAULT '0',
  `caln_squence` int(11) NOT NULL DEFAULT '0',
  `caln_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `caln_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`caln_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `compounds`
--

CREATE TABLE IF NOT EXISTS `compounds` (
  `comp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_landLordID` int(11) NOT NULL DEFAULT '0',
  `comp_numberOfHouses` int(11) NOT NULL DEFAULT '0',
  `comp_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comp_remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`comp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Cont_ContactInfoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_EntityID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_EntityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_Contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_ContactType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `Cont_ContactInfoID`, `Cont_EntityID`, `Cont_EntityType`, `Cont_Contact`, `Cont_ContactType`, `Cont_Deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, '4', 'Person', '7052177', 'phones', 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(2, NULL, '4', 'Person', 'ema@email', 'email', 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(3, NULL, '4', 'Person', '5465846', 'telephone', 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(4, NULL, '13', 'Person', '705214', 'phones', 0, '2014-08-22 01:58:49', '2014-08-22 01:58:49'),
(5, NULL, '14', 'Person', '705214', 'phones', 0, '2014-08-22 02:09:47', '2014-08-22 02:09:47'),
(6, NULL, '15', 'Person', '705214', 'phones', 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(7, NULL, '15', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(8, NULL, '15', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(9, NULL, '16', 'Person', '705214', 'phones', 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(10, NULL, '16', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(11, NULL, '16', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:11:15', '2014-08-22 02:11:15'),
(12, NULL, '17', 'Person', '705214', 'phones', 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(13, NULL, '17', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(14, NULL, '17', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(15, NULL, '18', 'Person', '705214', 'phones', 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(16, NULL, '18', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(17, NULL, '18', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(18, NULL, '19', 'Person', '705214', 'phones', 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(19, NULL, '19', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(20, NULL, '19', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(21, NULL, '20', 'Person', '705214', 'phones', 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(22, NULL, '20', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(23, NULL, '20', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(24, NULL, '21', 'Person', '705214', 'phones', 0, '2014-08-22 02:22:45', '2014-08-22 02:22:45'),
(25, NULL, '21', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:22:46', '2014-08-22 02:22:46'),
(26, NULL, '21', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:22:46', '2014-08-22 02:22:46'),
(27, NULL, '22', 'Person', '705214', 'phones', 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(28, NULL, '22', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(29, NULL, '22', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(30, NULL, '23', 'Person', '705214', 'phones', 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(31, NULL, '23', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(32, NULL, '23', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(33, NULL, '24', 'Person', '705214', 'phones', 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(34, NULL, '24', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(35, NULL, '24', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(36, NULL, '25', 'Person', '565464', 'phones', 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(37, NULL, '25', 'Person', 'sdfgsdgsgs', 'email', 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(38, NULL, '25', 'Person', '34535345453', 'telephone', 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(39, NULL, '26', 'Person', '676785676', 'phones', 0, '2014-08-22 02:46:59', '2014-08-22 02:46:59'),
(40, NULL, '30', 'Person', '234234234', 'phones', 0, '2014-08-22 16:37:07', '2014-08-22 16:37:07'),
(41, NULL, '30', 'Person', '23423423', 'telephone', 0, '2014-08-22 16:37:08', '2014-08-22 16:37:08'),
(42, NULL, '31', 'Person', '7052217', 'phones', 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(43, NULL, '31', 'Person', 'sulsira@something.com', 'email', 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(44, NULL, '31', 'Person', '765464546', 'telephone', 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(45, NULL, '32', 'Person', '7052217', 'phones', 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(46, NULL, '32', 'Person', 'sulsira@hotm.com', 'email', 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(47, NULL, '32', 'Person', '7654656', 'telephone', 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(48, NULL, '33', 'Person', '7052217', 'phones', 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(49, NULL, '33', 'Person', 'somethin@email.com', 'email', 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(50, NULL, '33', 'Person', '6677578', 'telephone', 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `cust_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cust_plotID` int(11) NOT NULL DEFAULT '0',
  `cust_downpayment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_partnerID` int(11) NOT NULL DEFAULT '0',
  `cust_personID` int(11) NOT NULL DEFAULT '0',
  `cust_ruleID` int(11) NOT NULL DEFAULT '0',
  `cust_transID` int(11) NOT NULL DEFAULT '0',
  `cust_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_lanID` int(11) NOT NULL DEFAULT '0',
  `cust_agenID` int(11) NOT NULL DEFAULT '0',
  `cust_payID` int(11) NOT NULL DEFAULT '0',
  `cust_deleted` int(11) NOT NULL DEFAULT '0',
  `cust_visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`cust_id`, `cust_plotID`, `cust_downpayment`, `cust_partnerID`, `cust_personID`, `cust_ruleID`, `cust_transID`, `cust_status`, `cust_lanID`, `cust_agenID`, `cust_payID`, `cust_deleted`, `cust_visible`, `created_at`, `updated_at`) VALUES
(1, 2, 'adfa', 0, 29, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-22 15:40:44', '2014-08-22 15:40:44'),
(2, 1, '200.3', 0, 30, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-22 16:37:08', '2014-08-22 16:37:08'),
(3, 3, '20000', 0, 31, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(4, 3, '2500', 0, 32, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(5, 5, '280000', 0, 33, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `exp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exp_houseID` int(11) NOT NULL DEFAULT '0',
  `exp_compoundID` int(11) NOT NULL DEFAULT '0',
  `exp_tenanceID` int(11) NOT NULL DEFAULT '0',
  `exp_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `exp_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`exp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE IF NOT EXISTS `houses` (
  `hous_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hous_compoundID` int(11) NOT NULL DEFAULT '0',
  `hous_tenantID` int(11) NOT NULL DEFAULT '0',
  `hous_advance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `hous_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `hous_paymentStype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hous_availability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hous_status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`hous_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `landlords`
--

CREATE TABLE IF NOT EXISTS `landlords` (
  `lrds_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lrds_fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`lrds_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_08_23_182322_add_paym_userID_to_payments_table', 1),
('2014_08_23_185052_add_paym_code_to_payments_table', 2),
('2014_08_24_065815_add_paym_plotID_to_table', 3),
('2014_08_29_220843_remove_fields_transactions_table', 4),
('2014_08_29_222506_add_fields_transactions_table', 5),
('2014_08_29_222718_remove_fields_payments_table', 6),
('2014_08_29_223402_add_fields_payments_table', 7),
('2014_08_29_235421_add_paym_paidAmount_to_payments_table', 8),
('2014_08_30_014012_remove_fields_plots_table', 9),
('2014_08_30_014453_add_plot_price_to_plots_table', 10),
('2014_08_31_013041_create_landlords_table', 11),
('2014_08_31_013104_create_compounds_table', 12),
('2014_08_31_013133_create_houses_table', 12),
('2014_08_31_013247_create_tenants_table', 12),
('2014_08_31_013814_create_expenses_table', 12),
('2014_08_31_013833_create_rents_table', 12),
('2014_08_31_013848_create_calender_Table', 12),
('2014_08_31_042259_create_Notifications_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entityID` int(11) NOT NULL DEFAULT '0',
  `entityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entityTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `par_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `par_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `par_relationship` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `par_payType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `par_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`par_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `paym_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paym_custID` int(11) NOT NULL DEFAULT '0',
  `paym_paidAmount` decimal(28,3) NOT NULL DEFAULT '0.000',
  `paym_plotID` int(11) NOT NULL DEFAULT '0',
  `paym_balance` decimal(28,3) NOT NULL DEFAULT '0.000',
  `paym_currentBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `paym_transDate` date DEFAULT NULL,
  `paym_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `paym_userID` int(11) NOT NULL DEFAULT '0',
  `paym_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`paym_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`paym_id`, `paym_custID`, `paym_paidAmount`, `paym_plotID`, `paym_balance`, `paym_currentBal`, `paym_transDate`, `paym_deleted`, `paym_userID`, `paym_code`, `created_at`, `updated_at`) VALUES
(1, 1, '0.000', 2, '0.000', '0.000', '0000-00-00', 0, 2, NULL, '2014-08-24 20:44:59', '2014-08-24 20:44:59'),
(2, 1, '0.000', 2, '0.000', '0.000', '0000-00-00', 0, 2, NULL, '2014-08-24 20:45:18', '2014-08-24 20:45:18'),
(3, 1, '0.000', 2, '0.000', '0.000', '0000-00-00', 0, 2, NULL, '2014-08-24 20:46:55', '2014-08-24 20:46:55'),
(4, 2, '0.000', 1, '0.000', '0.000', '0000-00-00', 0, 2, NULL, '2014-08-29 20:46:39', '2014-08-29 20:46:39'),
(5, 1, '23447.000', 2, '0.000', '0.000', '2014-08-20', 0, 2, NULL, '2014-08-30 01:19:12', '2014-08-30 01:19:12'),
(6, 5, '1000.000', 5, '0.000', '280000.000', '2014-08-31', 0, 2, NULL, '2014-08-30 02:09:58', '2014-08-30 02:09:58'),
(7, 5, '20.455', 5, '0.000', '280000.000', '2014-08-11', 0, 2, NULL, '2014-08-30 21:20:38', '2014-08-30 21:20:38');

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pers_indentifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_givenID` int(11) NOT NULL DEFAULT '0',
  `pers_fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_mname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_DOB` date DEFAULT NULL,
  `pers_gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_ethnicity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_standing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_NIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `pers_indentifier`, `pers_givenID`, `pers_fname`, `pers_mname`, `pers_lname`, `pers_type`, `pers_DOB`, `pers_gender`, `pers_nationality`, `pers_ethnicity`, `pers_standing`, `pers_NIN`, `pers_deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, 'asdfasdfa', NULL, 'asdfadsaf', 'Agent', '2014-08-19', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 00:55:43', '2014-08-22 00:55:43'),
(2, NULL, 0, 'asdfasdfa', NULL, 'asdfadsaf', 'Agent', '2014-08-19', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:03:26', '2014-08-22 01:03:26'),
(3, NULL, 0, 'first', 'middle', 'jallow', 'Agent', '2014-08-26', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:06:45', '2014-08-22 01:06:45'),
(4, NULL, 0, 'first', 'middle', 'jallow', 'Agent', '2014-08-26', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(5, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:29:50', '2014-08-22 01:29:50'),
(6, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:30:47', '2014-08-22 01:30:47'),
(7, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:32:17', '2014-08-22 01:32:17'),
(8, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:43:03', '2014-08-22 01:43:03'),
(9, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:46:37', '2014-08-22 01:46:37'),
(10, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:48:55', '2014-08-22 01:48:55'),
(11, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:57:52', '2014-08-22 01:57:52'),
(12, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:58:20', '2014-08-22 01:58:20'),
(13, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:58:49', '2014-08-22 01:58:49'),
(14, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:09:47', '2014-08-22 02:09:47'),
(15, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(16, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(17, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(18, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(19, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(20, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(21, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:22:45', '2014-08-22 02:22:45'),
(22, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(23, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(24, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(25, NULL, 0, 'dsfadsfa', 'adfa', 'asdfasf', 'Agent', '2014-08-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(26, NULL, 0, 'ebri', 'so', 'fasdl;fj', 'Staff', '2014-09-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:46:59', '2014-08-22 02:46:59'),
(27, NULL, 0, 'adsfas', NULL, 'asdfa', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 15:39:11', '2014-08-22 15:39:11'),
(28, NULL, 0, 'mamadou', NULL, 'jallow', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 15:40:00', '2014-08-22 15:40:00'),
(29, NULL, 0, 'mamadou', 's', 'jallow', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 15:40:44', '2014-08-22 15:40:44'),
(30, NULL, 0, 'ous', NULL, 'jallow', 'Customer', '2014-08-13', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 16:37:07', '2014-08-22 16:37:07'),
(31, NULL, 0, 'mamadou ', 's', 'jallow', 'Customer', '2014-08-27', 'female', 'Argentina', 'Sarahule', NULL, NULL, 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(32, NULL, 0, 'somethong', 'els', 'something', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(33, NULL, 0, 'theydont', 's', 'give', 'Customer', '2014-08-31', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `plots`
--

CREATE TABLE IF NOT EXISTS `plots` (
  `plot_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plot_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_price` decimal(28,3) NOT NULL DEFAULT '0.000',
  `plot_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_lon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_status` tinyint(1) NOT NULL DEFAULT '0',
  `plot_remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_availability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_cusID` int(11) NOT NULL DEFAULT '0',
  `plot_agenID` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`plot_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `plots`
--

INSERT INTO `plots` (`plot_id`, `plot_name`, `plot_size`, `plot_price`, `plot_location`, `plot_lon`, `plot_lat`, `plot_number`, `plot_status`, `plot_remarks`, `plot_availability`, `plot_cusID`, `plot_agenID`, `created_at`, `updated_at`) VALUES
(1, 'dsasdfas', 'asdfa', '0.000', 'adfa', NULL, NULL, 'adfafa', 0, 'asdfasdfa', NULL, 0, 0, '2014-08-21 23:10:45', '2014-08-21 23:10:45'),
(2, 'name', 'size', '0.000', 'location', NULL, NULL, 'number', 0, 'remraks', NULL, 0, 0, '2014-08-21 23:18:19', '2014-08-21 23:18:19'),
(3, 'the first one', '260 X 100', '0.000', 'kololi', NULL, NULL, 'adsf654316a4', 0, 'this is not yet sole', NULL, 0, 0, '2014-08-24 01:21:33', '2014-08-24 01:21:33'),
(4, 'point two ', '200 X 264', '1.544', 'kololi', NULL, NULL, '5455454545', 0, 'this plot is empty bla bla', NULL, 0, 0, '2014-08-30 01:47:22', '2014-08-30 01:47:22'),
(5, 'the plot', '28 X 30', '28000.000', 'serrekunda', NULL, NULL, 'saa28000', 0, 'asfasdfasdfafafa', NULL, 0, 0, '2014-08-30 02:07:05', '2014-08-30 02:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `rents`
--

CREATE TABLE IF NOT EXISTS `rents` (
  `rent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rent_houseID` int(11) NOT NULL DEFAULT '0',
  `rent_tenantID` int(11) NOT NULL DEFAULT '0',
  `rent_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `rent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE IF NOT EXISTS `tenants` (
  `tent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tent_houseID` int(11) NOT NULL DEFAULT '0',
  `tent_compoundID` int(11) NOT NULL DEFAULT '0',
  `tent_rentID` int(11) NOT NULL DEFAULT '0',
  `tent_advance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `tent_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `tent_paymentStype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tent_status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `trans_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_custID` int(11) NOT NULL DEFAULT '0',
  `trans_payID` int(11) NOT NULL DEFAULT '0',
  `trans_currentBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `trans_dueBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `trans_totalBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `trans_status` int(11) NOT NULL DEFAULT '0',
  `trans_visible` tinyint(1) NOT NULL DEFAULT '1',
  `trans_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `link`, `lickId`, `visible`, `deleted`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'sulsira@hotmail.com', '$2y$10$.3b1uD.DrBeqn9feJ1FgPey/qWkDuOLbrw9XgJcUt2g7U/mSbDso6', 0, 0, 0, 0, '4pYpSekUeC7LLm4CwkrXVml6lJYyFAEhw34DMLOB3Sbhjb86WB0OTk8caGGf', '2014-08-23 02:23:14', '2014-08-23 04:14:40'),
(2, NULL, 'admin@test.com', '$2y$10$MIXwV.1AID5.KUwlUs.dXuD0sIWEiJNU0OkOLGorgAnOCvGv4Ez2m', 0, 0, 0, 0, 'DRdvfT2llymxFkrQZQ8HLqg31pL9RfbatgCRQaLiVnGlMbpvtoRZNcpNOkQT', '2014-08-23 02:28:34', '2014-09-02 21:19:21'),
(3, NULL, 'suls@hot.com', NULL, 0, 0, 0, 0, NULL, '2014-08-29 20:49:12', '2014-08-29 20:49:12'),
(4, NULL, 'sulss@something.com', NULL, 0, 0, 0, 0, NULL, '2014-08-30 21:39:33', '2014-08-30 21:39:33'),
(5, NULL, 'adsfasd@dsaf.co', NULL, 0, 0, 0, 0, NULL, '2014-08-30 21:42:45', '2014-08-30 21:42:45'),
(6, NULL, 'asdfasdf@daf.com', '$2y$10$NotcVDSWE7cK9LrvnOBcveAzbiw7Z0KJc4GFsQiYNCY4uhUD4nAZC', 0, 0, 0, 0, NULL, '2014-08-30 21:44:22', '2014-08-30 21:44:22'),
(7, NULL, 'asdfa@dsfasd', '$2y$10$5FaQWsLjRQrvsEXZcWMb7.grWl7Qc/mBKw4QC0Wmu862luqIg6FsG', 0, 0, 0, 0, NULL, '2014-08-30 21:52:32', '2014-08-30 21:52:32'),
(8, NULL, 'sdfsad@dsaa', '$2y$10$UBMU5IOGsw7NI72KVfcUgeOyRV9lBbB2B6.T9fdLJn/6aVP1U.7Re', 0, 0, 0, 0, NULL, '2014-08-30 21:54:29', '2014-08-30 21:54:29'),
(9, NULL, 'sulsira@hotmail.com3', '$2y$10$L8tvS6hXfCW5fo9pW8YWVeO5io1D6jYFDg2dOdtAyhRgXCteNdfW6', 0, 0, 0, 0, NULL, '2014-08-30 23:11:15', '2014-08-30 23:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `privileges` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `type`, `fullname`, `ip`, `pc`, `privileges`, `department_id`, `userGroup`, `link`, `lickId`, `url`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'mamadou jallow', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-23 02:23:14', '2014-08-23 02:23:14'),
(2, 2, NULL, 'everyone account', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-23 02:28:34', '2014-08-23 02:28:34'),
(3, 3, NULL, 'mamadou jallow', '0', NULL, 'veda', NULL, 'agent', 0, 0, NULL, 0, '2014-08-29 20:49:12', '2014-08-29 20:49:12'),
(4, 4, NULL, 'saiho somehting', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-30 21:39:33', '2014-08-30 21:39:33'),
(5, 5, NULL, 'dfadsf afadf', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-30 21:42:45', '2014-08-30 21:42:45'),
(6, 6, NULL, 'sda asdfasdfa', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-30 21:44:22', '2014-08-30 21:44:22'),
(7, 7, NULL, 'ewrwa adsasfas', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-30 21:52:32', '2014-08-30 21:52:32'),
(8, 8, NULL, 'saf adsfa adsfasdf', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-30 21:54:29', '2014-08-30 21:54:29'),
(9, 9, NULL, 'sulsira  sdfm', '0', NULL, 'veda', NULL, 'admin', 0, 0, NULL, 0, '2014-08-30 23:11:15', '2014-08-30 23:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE IF NOT EXISTS `variables` (
  `Vari_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vari_VariableName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vari_Table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vari_Field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Vari_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=502 ;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`Vari_ID`, `Vari_VariableName`, `Vari_Table`, `Vari_Field`, `created_at`, `updated_at`, `Deleted`) VALUES
(50, 'University', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(51, 'College', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(52, 'Vocational', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(53, 'Technical', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(54, 'Other', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(55, 'Public', 'LearningCenter', 'LeCe_Ownership', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(56, 'Private', 'LearningCenter', 'LeCe_Ownership', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(58, 'Full Time', 'Class', 'Clas_AttendanceStatus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(59, 'Part Time', 'Class', 'Clas_AttendanceStatus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(60, 'Male', 'Student', 'Stud_Sex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(61, 'Female', 'Student', 'Stud_Sex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(62, 'GABECE', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(63, 'WASSCE', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(64, 'O ', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(65, 'A', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(66, 'Pre-Entry', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(67, 'Bachelor', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(68, 'Government', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(69, 'Private', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(70, 'NGO', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(71, 'Other', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(76, 'Professor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(77, 'Assoc. Professor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(78, 'Senior Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(79, 'Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(80, 'Asst. Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(82, 'Chief Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(83, 'Principal Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(84, 'Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(85, 'Asst. Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(91, 'Admin/Support- Full Time', 'Staff', 'Staff_Role/Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(92, 'Admin/Support- Part Time', 'Staff', 'Staff_Role/Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(93, 'Learning Center', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(94, 'Student', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(95, 'Staff', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(96, 'Mobile', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(97, 'LAN Line', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(98, 'FAX', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(100, 'Gambia, The', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(101, 'Senegal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(102, 'Algeria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(103, 'Angola', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(104, 'Anguilla', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(105, 'Antigua and Barbuda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(106, 'Argentina', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(107, 'Armenia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(108, 'Aruba', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(109, 'Australia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(110, 'Austria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(111, 'Azerbaijan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(112, 'Bahamas', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(113, 'Bahrain', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(114, 'Bangladesh', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(115, 'Barbados', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(116, 'Belarus', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(117, 'Belgium', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(118, 'Belize', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(119, 'Benin', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(120, 'Bermuda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(121, 'Bhutan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(122, 'Bolivia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(123, 'Bosnia and Herzegovina', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(124, 'Botswana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(125, 'Brazil', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(126, 'British Virgin Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(127, 'Brunei Darussalam', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(128, 'Bulgaria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(129, 'Burkina Faso', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(130, 'Burundi', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(131, 'Cambodia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(132, 'Cameroon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(133, 'Canada', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(134, 'Cape Verde', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(135, 'Cayman Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(136, 'Central African Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(137, 'Chad', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(138, 'Chile', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(139, 'China', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(140, 'Colombia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(141, 'Comoros', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(142, 'Congo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(143, 'Cook Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(144, 'Costa Rica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(145, 'C', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(146, 'Croatia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(147, 'Cuba', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(148, 'Cyprus', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(149, 'Czech Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(150, 'Demo. Repub. of the Congo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(151, 'Denmark', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(152, 'Dominica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(153, 'Dominican Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(154, 'Ecuador', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(155, 'Egypt', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(156, 'El Salvador', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(157, 'Equatorial Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(158, 'Eritrea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(159, 'Estonia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(160, 'Ethiopia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(161, 'EUROPE not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(162, 'Fiji', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(163, 'Finland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(164, 'France', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(165, 'Gabon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(167, 'Georgia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(168, 'Germany', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(169, 'Ghana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(170, 'Gibraltar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(171, 'Greece', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(172, 'Grenada', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(173, 'Guatemala', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(174, 'Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(175, 'Guinea-Bissau', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(176, 'Guyana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(177, 'Haiti', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(178, 'Holy See', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(179, 'Honduras', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(180, 'Hong Kong (SAR)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(181, 'Hungary', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(182, 'Iceland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(183, 'India', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(184, 'Indonesia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(185, 'Iran (Islamic Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(186, 'Iraq', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(187, 'Ireland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(188, 'Israel', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(189, 'Italy', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(190, 'Jamaica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(191, 'Japan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(192, 'Jordan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(193, 'Kazakhstan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(194, 'Kenya', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(195, 'Kiribati', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(196, 'Korea (Democratic People', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(197, 'Korea (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(198, 'Kuwait', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(199, 'Kyrgyzstan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(200, 'Lao People', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(201, 'Latvia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(202, 'Lebanon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(203, 'Lesotho', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(204, 'Liberia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(205, 'Libyan Arab Jamahiriya', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(206, 'Liechtenstein', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(207, 'Lithuania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(208, 'Luxembourg', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(209, 'Macao (China)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(210, 'Madagascar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(211, 'Malawi', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(212, 'Malaysia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(213, 'Maldives', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(214, 'Malta', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(215, 'Marshall Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(216, 'Mauritania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(217, 'Mauritius', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(218, 'Mexico', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(219, 'Micronesia (Federal States of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(220, 'Moldova (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(221, 'Monaco', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(222, 'Mongolia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(223, 'Montserrat', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(224, 'Morocco', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(225, 'Mozambique', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(226, 'Myanmar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(227, 'Namibia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(228, 'Nauru', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(229, 'Nepal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(230, 'Netherlands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(231, 'Netherlands Antilles', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(232, 'New Zealand', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(233, 'Nicaragua', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(234, 'Niger', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(235, 'Nigeria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(236, 'Niue', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(237, 'NORTH AMERICA not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(238, 'Norway', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(239, 'OCEANIA not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(240, 'Oman', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(241, 'Pakistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(242, 'Palau (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(243, 'Palestinian Autonomous Territories', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(244, 'Panama', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(245, 'Papua New Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(246, 'Paraguay', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(247, 'Peru', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(248, 'Philippines', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(249, 'Poland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(250, 'Portugal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(251, 'Qatar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(252, 'Romania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(253, 'Russian Federation', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(254, 'Rwanda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(255, 'Saint Kitts and Nevis', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(256, 'Saint Vincent and the Grenadines', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(257, 'Samoa', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(258, 'San Marino', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(259, 'Sao Tome and Principe', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(260, 'Saudi Arabia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(262, 'Serbia and Montenegro', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(263, 'Seychelles', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(264, 'Sierra Leone', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(265, 'Singapore', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(266, 'Slovakia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(267, 'Slovenia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(268, 'Solomon Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(269, 'Somalia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(270, 'South Africa', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(271, 'Spain', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(272, 'Sri Lanka', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(273, 'Sudan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(274, 'Suriname', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(275, 'Swaziland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(276, 'Sweden', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(277, 'Switzerland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(278, 'Syrian Arab Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(279, 'Tajikistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(280, 'Thailand', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(281, 'Republic of Macedonia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(282, 'Timor-Leste', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(283, 'Togo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(284, 'Tokelau', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(285, 'Tonga', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(286, 'Trinidad and Tobago', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(287, 'Tunisia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(288, 'Turkey', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(289, 'Turkmenistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(290, 'Turks and Caicos Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(291, 'Tuvalu', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(292, 'Uganda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(293, 'Ukraine', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(294, 'United Arab Emirates', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(295, 'United Kingdom', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(296, 'United Republic of Tanzania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(297, 'United States of America', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(298, 'Uruguay', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(299, 'Uzbekistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(300, 'Vanuatu', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(301, 'Venezuela', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(302, 'Viet Nam', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(303, 'Yemen', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(304, 'Zambia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(305, 'Zimbabwe', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(306, 'Certificate', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(307, 'Ordinary Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(308, 'Higher Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(309, 'Active', 'Standing', 'Standing', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(310, 'Inactive', 'Standing', 'Standing', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(311, 'Certificate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(312, 'Ordinary Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(313, 'Bachelor', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(314, 'Bachelor', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(315, 'Post Graduate Certificate', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(316, 'Post Graduate Diploma', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(317, 'Masters', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(318, 'Doctorate', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(320, '6', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(321, '12', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(322, '18', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(323, '24', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(324, '36', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(325, '48', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(326, '84', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(327, 'Other', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(328, 'Advanced Diploma\r\nOrdinary Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(329, 'Bachelor', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(330, 'Masters', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(331, 'Horticulture', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(332, 'Electrical Installation', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(333, 'Carpentry', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(334, 'Building Construction', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(335, 'Surveying', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(336, 'Food Processing', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(337, 'Hospitality', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(338, 'Architecture', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(339, 'Hairdressing', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(340, 'Animal Husbandry', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(341, 'Basic Literacy', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(343, 'Certificate', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(344, 'Diploma', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(345, 'Student', 'Person', 'Pers_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(346, 'Staff', 'Person', 'Pers_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(347, 'Motor Vehicle Systems', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(349, 'Plumbing and Gas Fitting', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(350, 'Cooking and Baking', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(351, 'Sewing/Craft Work and Tie Dye', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(352, 'Home Economics', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(353, 'House Keeping', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(354, 'Refrigeration and Air Conditioning', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(355, 'Painting', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(356, 'Graphic Design', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(357, 'Draftsmanship', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(358, 'Secretarial', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(359, 'Computer Repair', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(360, 'Fashion Design', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(361, 'Mobile Repair', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(362, '1', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(363, '2', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(364, '3', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(365, '4', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(366, 'Basic Programmes', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(367, 'Teacher training and education science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(368, 'Education Science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(369, 'Arts', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(370, 'Humanities', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(371, 'Social and Behavioural Science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(372, 'Journalism and Information', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(373, 'Business and Administration', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(374, 'Law', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(375, 'Life Sciences', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(376, 'Physical Sciences', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(377, 'Mathematics and Statistics', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(378, 'Computing', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(379, 'Engineering and Engineering Trades', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(380, 'Manufacturing and Processing', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(381, 'Architecture and Building', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(382, 'Agriculture, Forestry and Fishery', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(383, 'Veterinary', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(384, 'Health', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(385, 'Social Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(386, 'Personal Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(387, 'Transport Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(388, 'Environmental Protection', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(389, 'Security Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(390, 'Unspecified', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(391, 'General Programmes', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(392, 'Education', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(393, 'Humanities and Arts', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(394, 'Social Sciences', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(395, 'Physical and Natural Science', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(396, 'Engineering, Manufacturing and Construction', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(397, 'Agriculture', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(398, 'Public and Medical Health Science', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(399, 'Tourism/Hospitality/Services', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(400, 'Not Known or Unspecified', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(401, 'Permanent', 'Staff', 'Staff_EmploymentType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(402, 'Temporary', 'Staff', 'Staff_EmploymentType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(403, 'Masters', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(404, 'Doctorate (PhD)', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(405, 'Post Graduate Certificate', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(406, 'Administraive staff', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(407, 'Instructor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(408, 'J.S.S Certificate', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(409, 'Business', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(410, 'Law', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(412, 'Information and Communication Technology', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(413, 'Basic Certificate', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(414, 'Certificate', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(415, 'Diploma	Student', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(416, 'Bachelor', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(417, 'Masters	Student', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(418, 'Doctorate', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(419, 'Post Graduate Certificate', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(420, 'Post Graduate Diploma', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(421, 'Phone', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(422, 'Email', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(423, 'Academic', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(424, 'Administrative', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(425, 'Both', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(426, 'Post Graduate Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(430, 'Banjul South', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(431, 'Banjul Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(432, 'Banjul North', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(433, 'Serre Kunda East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(434, 'Serre Kunda West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(435, 'Serre Kunda Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(436, 'Bakau', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(437, 'Kombo North', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(438, 'Kombo South', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(439, 'Kombo Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(440, 'Kombo East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(441, 'Foni Brefet', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(442, 'Foni Bintang', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(443, 'Foni Kansalla', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(444, 'Foni Bondali', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(445, 'Foni Jarrol', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(446, 'Kiang West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(447, 'Kiang Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(448, 'Kiang East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(449, 'Jarra West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(450, 'Jarra Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(451, 'Jarra East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(452, 'Lower Nuimi', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(453, 'Upper Nuimi', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(454, 'Jokadu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(455, 'Lower Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(456, 'Central Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(457, 'Upper Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(458, 'Niani', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(459, 'Lower Saloum', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(460, 'Upper Saloum', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(461, 'Nianija', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(462, 'Sami', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(463, 'Niamina West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(464, 'Niamina East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(465, 'Niamina Dankunko', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(466, 'Janjanbureh', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(467, 'Fuladu East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(468, 'Kantora', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(469, 'Wuli', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(470, 'Sandu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(471, 'Fuladu West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(475, 'Web site', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(476, 'Fula', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(477, 'Jola', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(478, 'Mandinka', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(479, 'Manjago', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(480, 'Sarahule', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(481, 'Serere', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(482, 'Tukulor', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(483, 'Wollof', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(484, 'Other', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(485, 'Fax', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(486, 'N/A', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(487, 'Masters', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(488, 'Doctorate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(489, 'Post Graduate Certificate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(490, 'Post Graduate Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(491, 'Higher Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(492, 'Advanced Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(493, 'Higher Education', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(494, 'Tertiary', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(495, 'TVET', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(496, 'Internal', 'Users', 'User_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(497, 'External', 'Users', 'User_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(498, 'Basic Literacy', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(499, 'Public', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(500, 'Government', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(501, 'Both', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
