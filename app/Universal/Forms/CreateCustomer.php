<?php namespace Universal\Forms;

use Laracasts\Validation\FormValidator;

class CreationCustomer extends FormValidator{


		/**
		 * validation rules for the plot creation form
		 * Post /plots/create
		 *
		 * @return Response 
		 */
	protected $rules = [
		'plot_name'=>'required|max:200',
		'plot_size'=>'max:200',
		'plot_price'=>'max:200',
		'plot_location'=>'max:200',
		'plot_number'=>'max:200',
		'plot_remarks'=>'max:200'
	];


}