<?php

class HousesController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /houses
	 *
	 * @return Response
	 */
	public function index()
	{
		// $all = Compound::with('houses')->get();
		$all = House::with('compound.landlord')->get();
		$all = ($all) ? $all->toArray() : [];
		$this->layout->content = View::make('admin.Houses.index')->with('houses',$all);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /houses/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.Houses.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /houses
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		// no validation
		$house = House::create(array(
		'hous_compoundID'=> $input['house_compound'],
		'hous_advance'=> $input['house_advance'],
		'hous_price'=> $input['house_price'],
		'hous_description'=> $input['house_remarks'],
		'hous_number'=> $input['house_number'],
		'hous_numberOfrooms'=> $input['room_number']
		));

		if( $house ){
				Flash::message("Successfully added a House");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
	}

	/**
	 * Display the specified resource.
	 * GET /houses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// ->with('houses',$all)
		$house = House::with('compound','tenants.person')->first();
		$house = (!empty($house)) ? $house->toArray() : [];
		// dd($house);
		$this->layout->content = View::make('admin.Houses.show')->with('house', $house);
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /houses/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('admin.Houses.edit');
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /houses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$house = House::find($id);
		$house->fill($input);
		$house->save();
		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /houses/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		House::destroy($id);
		return Redirect::back();
	}

}