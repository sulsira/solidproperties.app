<?php 
use services\menus\Topbar;
class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin
	 *
	 * @return Response
	 */
	protected $layout = 'admin';
	protected $menu = array();
	protected $data = array();
	protected $plotAttr;
	private $something;
	public function __construct(){
		$user = User::find(Auth::user()->id)->role()->first();

		// $this->something = new Menu();
		$this->data['userDetails'] = $user;
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/admin','priv'=>'veda','type'=>'single'],
			['name'=>'land lords','visible'=> 1 , 'url'=>'/land-lords','priv'=>'veda','type'=>'single'],
			['name'=>'compounds','visible'=> 1 , 'url'=>'/compounds','priv'=>'veda','type'=>'single'],
			['name'=>'houses','visible'=> 1 , 'url'=>'/houses','priv'=>'veda','type'=>'single'],
			['name'=>'tenants','visible'=> 1 , 'url'=>'/tenants','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 1 , 'url'=>'/notifications','priv'=>'veda','type'=>'single'],
			['name'=>'home','visible'=> 0 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'home','visible'=> 0 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'home','visible'=> 0 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'estates','visible'=> 1 , 'url'=>'/estates','priv'=>'veda','type'=>'single'],
			['name'=>'agents','visible'=> 1 , 'url'=>'/agents','priv'=>'veda','type'=>'single'],
			['name'=>'staff','visible'=> 1 , 'url'=>'/staffs','priv'=>'veda','type'=>'single'],
			['name'=>'customers','visible'=> 1 , 'url'=>'/customers','priv'=>'veda','type'=>'single'],
			['name'=>'partners','visible'=> 0 , 'url'=>'/partners','priv'=>'veda','type'=>'single'],
			['name'=>'transactions','visible'=> 1 , 'url'=>'transactions','priv'=>'veda','type'=>'single'],
			['name'=>'Prospectives','visible'=> 0 , 'url'=>'prospectives','priv'=>'veda','type'=>'single'],
			['name'=>'Tenants','visible'=> 0 , 'url'=>'tenants','priv'=>'veda','type'=>'single'],
			['name'=>'Users','visible'=> 1 , 'url'=>'users','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 0 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'users','visible'=> 0 , 'url'=>'admin/users','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$this->data['userDetails']['fullname'],'visible'=> 1 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data = $this->menu;
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});		

	}
	public function index()
	{	
		// new Topbar();
		// $menu = new Topbar(['something','somethielse']);
		// dd($menu->getMenu());
		$this->layout->content = View::make('admin.index')->with('data',$this->data);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}