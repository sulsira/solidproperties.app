<?php #page specific processing

$tenants = array();
$tenants = array();
$payments = array();
 $comps = Compound::all();
 $compounds = ($comps)? $comps->toArray() : [];
// dd($compounds);
// die();
 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-edit-house')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="#">House Number : <?php echo (!empty($house['hous_number']))? $house['hous_number'] : 'No number'; ?></a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="{{route('houses.show',$house['hous_id'])}}#details">Details</a> </li>
                            <li><a href="{{route('houses.show',$house['hous_id'])}}#tenants">Tenants</a> </li>
                            <li><a href="{{route('houses.show',$house['hous_id'])}}#payments">Payments</a></li>
                            <li><a href="#edit-house" role="button" data-toggle="modal">Edit</a></li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div>            
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
  <div class="cc clearfix" id="details">
    <h3>House details</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Number</th>
            <th>Price</th>
            <th>House advance</th>
            <th>Available</th>
            <th>Number of rooms</th>
            <th>Description</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($house)): ?>

            <tr>
                <td>{{ucfirst($house['hous_number'])}}</td> 
                <td>{{ucfirst($house['hous_price'])}}</td> 
                <td>{{ucfirst($house['hous_advance'])}}</td> 
                <td>{{ucfirst($house['hous_availability'])}}</td> 
                <td>{{ucfirst($house['hous_numberOfrooms'])}}</td> 
                <td>{{ucfirst($house['hous_description'])}}</td> 
                <td>{{ucfirst($house['created_at'])}}</td> 
            </tr>                                              
        <?php else: ?>
        <tr><td colspan="8"><h4>no house details yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
   <div class="cc clearfix" id="tenants">
        <h3>Tenants</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Fullname</th>
            <th>Gender</th>
            <th>D.O.B</th>
            <th>Nationality</th>
            <td>Status</td>
            <th>Tent advance</th>
            <th>Monthly fee</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($house['tenants'])): ?>
          <?php foreach ($house['tenants'] as $t => $tenant): ?>
            <tr>
                <td><a href="{{route('tenants.show',$tenant['tent_id'])}}"><?php echo ucfirst($tenant['person']['pers_fname'].' '.$tenant['person']['pers_mname'].' '.$tenant['person']['pers_lname']) ?></a></td>
                <td>{{$tenant['person']['pers_gender']}}</td>
                <td>{{$tenant['person']['pers_DOB']}}</td>
                <td>{{$tenant['person']['pers_nationality']}}</td>
                <td><?php if ($tenant['tent_status'] == 0): ?>
                  Vacated
                <?php else: ?>
                  <?php if ( $tenant['tent_status'] == 1 ): ?>
                     Occupant
                  <?php endif ?>
                  <?php if ( $tenant['tent_status'] == 2 ): ?>
                     Pending to Leave
                  <?php endif ?>
                  <?php if ( $tenant['tent_status'] == 3 ): ?>
                     Vacated but owing
                  <?php endif ?>
                <?php endif ?></td>
                <td>{{$tenant['tent_advance']}}</td>
                <td>{{$tenant['tent_monthlyFee']}}</td>
                <td>{{$tenant['created_at']}}</td>
            </tr>                                    
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="8"><h4>no tenants details yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>
   <div class="cc clearfix" id="payments">
        <h3>Payments</a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Amount</th>
            <th>For : </th>
            <th>Payment Date</th>
            <th>Paid by</th>
            <th>Track</th>
            <th>Description</th>
            <th>updated</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
            <tr>
                <td>{{$plot['plot_name']}}</td>
                <td>{{$plot['plot_number']}}</td>
                <td>{{$plot['plot_size']}}</td>
                <td>{{$plot['plot_price']}}</td>
                <td>{{$plot['plot_location']}}</td>
                <td>{{$plot['plot_status']}}</td>
                <td>{{$plot['plot_availability']}}</td>
                <td>{{$plot['plot_remarks']}}</td>
                <td>{{$plot['created_at']}}</td>
                <td><a href="{{route('plots.show',$plot['plot_id'])}}">view</a> | <a href="#">options</a></td>
            </tr>                                                
            <?php endforeach ?>  
        <?php else: ?>
       <tr><td colspan="8"><h4>no payments yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>
</div>


@stop
@include('templates/bottom-admin')