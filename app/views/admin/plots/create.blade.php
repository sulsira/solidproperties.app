@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-Plot">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Create A New Plot </h2>
					</div>
				</div>
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				{{Form::open(['route'=>'plots.store'],[],['class'=>'form-snippet'])}}
					<div class="level name">
						<div>
							{{Form::label('est_name','Estate Name')}}
							{{Form::text('est_name',null,['class'=>'input-xlarge span6','placeholder'=>'Enter the Plot name','required'=>1])}}
						</div>
						<div>
							{{Form::label('plot_location','Location')}}
							{{Form::text('plot_location',null,['class'=>'input-xlarge span6','placeholder'=>'Location or Adress'])}}
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('plot_price','Plot Price (In Dalasis)')}}
							{{Form::number('plot_price',['class'=>'input-xlarge span6','placeholder'=>'Enter the Plot price','step'=>'any'])}}
						</div>
						<div>
							{{Form::label('plot_size','Plot Size')}}
								<select name="plot_size" class="span6">
									<?php $plots = Variable::domain('plot_size')->toArray();  ?>
									@foreach ($plots as  $key => $plot)
									<option>{{$plot['Vari_VariableName']}}</option>
									@endforeach
								</select>
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('plot_number','Plot Number')}}
							{{Form::text('plot_number',null,['class'=>'input-xlarge span12','placeholder'=>'A unique Plot number'])}}
						</div>

					</div>
					<div class="level">
						<div>
							{{Form::label('plot_remarks','Plot Remarks')}}
							{{Form::textarea('plot_remarks',null,['class'=>'input-xlarge span12','placeholder'=>'Enter your remarks here'])}}
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span12" name="save" value="save">Create Plot</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')