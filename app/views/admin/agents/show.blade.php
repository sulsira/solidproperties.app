<?php #page specific processing
    $unassigned  = Plot::unassigned();
    $unassigned  = (!empty($unassigned))? $unassigned->toArray() : [];
    $plots = (!empty($agent['plots']))? $agent['plots'] : array();
    $customers = array();
    $empty_plots = array();
    $person = array();
    $image = array();
    if(isset($agent) && !empty($agent)):
        $person = $agent['person'];
        $addresses = $person['addresses'];
        $contacts = $agent['person']['contacts'];
        $fullname = ucwords($person['pers_fname'] .'  '. $person['pers_mname'].' '.$person['pers_lname']);
    endif;
    foreach ($plots as $p => $plot) {
       if (empty($plot['customer'])) {
          $empty_plots[] = $plot;
       }else{
        $customers[] = $plot;
       }
    }
    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }
    // dd($image);
 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-aplot')
@include('__partials/modal-add-Acustomer')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Agent Name : {{ucwords("{ $fullname }")}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="#index">Basic</a> </li>
                            <li><a href="#transactions">Plots</a> </li>
                            <li><a href="#transactions">Customers</a> </li>
                            <li><a href="{{route('agents.edit',$agent['agen_id'])}}">Edit</a></li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul> 

                <?php endif ?>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
            <div class="cc clearfix" >
                <hr>
                <h3>Basic information</h3>
                <hr id="basic">
                <div class="span8 clearfix">
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($fullname)}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($person['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($person['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($person['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($person['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>

                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($contacts)): ?>
                                            <?php foreach ($contacts  as $key => $value): ?>
                                                <tr>
                                                    <td>{{ucwords($value['Cont_ContactType'])}}:</td>
                                                    <td>{{ucwords($value['Cont_Contact'])}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($addresses)): ?>
                                            <?php foreach ($addresses as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <!-- should change -->
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Plots</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($customer['plots'])): ?>
                                          <?php foreach ($customer['plots'] as $key => $plot): ?>
                                            <tr>
                                                <td>name:</td>
                                                <td>{{ucwords($plot['plot_name'])}}</td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>                                                
                                            <?php endforeach ?>  
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div> <!-- a .cc -->

   <div class="cc clearfix" id="plots">
        <h3>Plots <a href="#add-Apot" role="button" data-toggle="modal"> <i class="fa fa-plus"></i></a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>plot estate</th>
            <th>plot number</th>
            <th>plot size</th>
            <th>plot price</th>
            <th>location</th>
            <th>availability</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($empty_plots)): ?>
          <?php foreach ($empty_plots as $ep => $plot): ?>
            <tr>
                <td>
                    <a href="{{route('estates.plots.index',$plot['estate']['est_id'])}}">
{{ucwords($plot['estate']['name'])}}
                    </a>
                </td>
                <td>
                    {{e(ucwords($plot['plot_number']))}}
                </td>
                <td>
                    {{e(ucwords($plot['plot_size']))}}
                </td>
                <td>
                    {{e(ucwords($plot['plot_price']))}}
                </td>
                <td>
                    {{e(ucwords($plot['plot_location']))}}
                </td>
                <td>
                    {{e(ucwords($plot['plot_availability']))}}
                </td>
                <td>
                    {{e(ucwords($plot['created_at']))}}
                </td>
            </tr>                                                
            <?php endforeach ?>  
        <?php else: ?>
                    <tr>
                        <td colspan="7"><h4>No Plots Available!</h4></td>
                    </tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>

   <div class="cc clearfix" id="plots">
        <h3>Customers <a href="{{route('agents.customers.create',1)}}"> <i class="fa fa-plus"></i></a></h3>
    <hr>
        <table class="table">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Nationality</th>
                    <th>Plot Number</th>
                    <th>Gender</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($customers)): ?>
                    <?php foreach ($customers as $c => $customer): ?>
                    <tr>
                        <td><a href="{{route('customers.show',$customer['customer']['cust_id'])}}">
                        <?php 
echo ucwords($customer['customer']['person']['pers_fname'].' '.$customer['customer']['person']['pers_mname'].' '.$customer['customer']['person']['pers_lname']);
                         ?>
                        </a></td>
                        <td>{{ucfirst($customer['customer']['person']['pers_nationality'])}}</td>
                        <td><a href="{{route('plots.show',$customer['plot_id'])}}">{{ucfirst($customer['plot_number'])}}</a></td>
                        <td>{{ucfirst($customer['customer']['person']['pers_gender'])}}</td>
                    </tr>
                    <?php endforeach ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="7"><h4>No Customer Available!</h4></td>
                    </tr>
                <?php endif ?>
            </tbody>
        </table>
   </div>

</div>


@stop
@include('templates/bottom-admin')