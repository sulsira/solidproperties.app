<?php #page specific processing
$image = array();
$person =  array();
$contacts = array();
$documents =  array();
$fullname = '';
$customer_id = $customer['cust_id'];
$kin = (!empty($customer['kin']))? $customer['kin'] : [];
    if(isset($customer) && !empty($customer)):
        $person = $customer['person'];
        $addresses = $person['addresses'];
        $contacts = $customer['person']['contacts'];
        $fullname = ucwords($person['pers_fname'] .'  '. $person['pers_mname'].' '.$person['pers_lname']);
        foreach ($customer as $key => $value) {
           if ($key == 'person') {
              $person = $value;
           }
        }
    endif;

    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }

 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-landpayment')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Customer Name : {{ucwords("{ $fullname }")}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="#basic">General</a> </li>
                            <li><a href="#transactions">Transactions</a></li>
                            <li><a href="#payments">Payments</a></li>
                            <li><a href="#plots">Plots</a></li>
                            <li><a href="{{route('customers.edit',$customer['cust_id'])}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul> 

                <?php endif ?>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
    	<div class="cc clearfix">
    		    		<h3>Editing Tenant</h3>
    		<hr>

	        <ul class="thumbnails">
	          <li class="row">
	            <div class="thumbnail">
	              <div class="caption">
	                <h3>Edit Personal</h3>
	                <hr>
				{{Form::model($person, ['route'=>[ 'customers.update' , $customer['cust_id']  ], 'method'=>'PATCH'],['class'=>'form-snippet'])}}

					<div class="level details">

						<div class="first">
							<div>
								{{Form::label('pers_fname','First Name')}}
								{{Form::text('pers_fname',null,['class'=>'input-xlarge','placeholder'=>'Enter first name','required'=>1])}}
							</div>
							<div>
								{{Form::label('pers_mname','Middle Name')}}
								{{Form::text('pers_mname',null,['class'=>'input-xlarge','placeholder'=>'Enter middle name'])}}
							</div>
							<div>
								{{Form::label('pers_lname','Last Name')}}
								{{Form::text('pers_lname',null,['class'=>'input-xlarge','placeholder'=>'Enter last name','required'=>1])}}
							</div>							
						</div>
					</div>
					<div class="level details">
						<span>Bio </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('pers_DOB','Birthday ( year-month-day )')}}
								{{Form::text('pers_DOB',null,['class'=>'input-xlarge' ])}}
							</div>
							<div>
								{{Form::label('pers_ethnicity','Ethniticity')}}
								<select name="pers_ethnicity" id="enit" class="input-xlarge">
									<?php $countries = Variable::domain('Pers_Ethnicity')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select>
							</div>
							<div>
								{{Form::label('pers_nationality','Nationality')}}
								<select name="pers_nationality" id="nation" class="input-xlarge">
									<?php $countries = Variable::domain('Country')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select>
							</div>
							<div>
								{{Form::label('pers_gender','Gender')}}
								{{Form::select('pers_gender', array('male' => 'Male','female' => 'Female'));}}
							</div>
						</div>
					</div>

					<div class="level details">
						<span>Identification details</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('pers_NIN','Nation id number / passport number')}}
								{{Form::text('pers_NIN',null,['class'=>'input-xlarge span6','placeholder'=>'NIN number or passport number'])}} 
							</div>
						</div>
					</div>

	                <hr>
					{{Form::hidden('type','person')}} 
					{{Form::hidden('person_id',$person['id'])}} 
	                <button class="btn btn-primary">Save Changes</button>
					<button type="reset" class="btn">reset</button>
	              </div>
	            </div>
	            {{Form::close()}}
	          </li>  
	          
	        </ul>
	        <ul class="thumbnails">
<li class="span7">
	            <div class="thumbnail">
	              <div class="caption">
	                <h3>Edit Address</h3>
	                <hr>
						{{Form::model($person['addresses'][0], ['route'=>[ 'customers.update' , $person['addresses'][0]['Addr_AddressID'] ], 'method'=>'PATCH'],['class'=>'form-snippet'])}}
						<div class="level details">
							<span>Address</span>
							<hr>
							<div class="first ">
								<div>
									{{Form::label('Addr_Town','Town')}}
									{{Form::text('Addr_Town',null,['class'=>'input-xlarge','placeholder'=>'Enter town'])}}
								</div>
								<div>
									{{Form::label('Addr_District','District')}}
									<select name="Addr_District" id="region">
										<?php $countries = Variable::domain('Addr_District')->toArray();  ?>
										@foreach ($countries as $key => $country)
										<option>{{$country['Vari_VariableName']}}</option>
										@endforeach
									</select> 
								</div>
								<div>
									{{Form::label('Addr_AddressStreet','street')}}
									{{Form::text('Addr_AddressStreet',null,['class'=>'input-xlarge','placeholder'=>'Enter street'])}}
								</div>
							</div>
						</div>
	                <hr>
	                {{Form::hidden('type','address')}} 
					{{Form::hidden('address_id', $person['addresses'][0]['Addr_AddressID'] )}} 
	                <button class="btn btn-primary">Save Changes</button>
					<button type="reset" class="btn">reset</button>
	              </div>
	            </div>
	            {{Form::close()}}
	          </li> 
	          <li class="span7">
	            <div class="thumbnail">
      			{{HTML::image($image['thumnaildir'])}}
	              <div class="caption">
				{{Form::model($image, ['route'=>['customers.update', $customer['cust_id'] ], 'files'=>true,'method'=>'PATCH'],['class'=>'form-snippet'])}}
					{{Form::hidden('foldername')}}
					{{Form::hidden('fullpath')}}
					{{Form::hidden('thumnaildir')}}
					<div class="level details">
						<span>Profile picture</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('photo','Photo')}}
								{{Form::file('photo')}}
							</div>	
						</div>
					</div>

	                <hr>
	                {{Form::hidden('type','image')}} 
					{{Form::hidden('image_id', $image['id'])}} 
	                <button class="btn btn-primary">Save Changes</button>
					<button type="reset" class="btn">reset</button>
	           
	              </div>
	            </div>
	             {{Form::close()}}
	          </li>   
	        </ul>
    	</div>
	</div>
@stop
@include('templates/bottom-admin')