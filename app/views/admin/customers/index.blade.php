@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Customers</h3>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Customer Name</th>
					<th>Nationality</th>
					<th>Plot Number</th>
					<th>Gender</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($customers)): ?>
					<?php foreach ($customers as $key => $value): ?>
					<tr>
						<td>{{$key+1}}</td>
						<td>
<a href="{{route('customers.show',$value['cust_id'])}}"><?php echo ucwords($value['person']['pers_fname'] .'  '. $value['person']['pers_mname'].' '.$value['person']['pers_lname']) ?>
</a>
						</td>
							<td>
<?php echo ucwords($value['person']['pers_nationality']) ?>
							</td>
<td>
<?php if ( isset($value['plots']) && !empty($value['plots']) ): ?>
	<ul>
		<?php foreach ($value['plots'] as $plots): ?>

			<?php if (!empty($plots)): ?>
				<li>
					<a href="{{route('plots.show',$plots['plot_id'])}}"><?php echo ucwords($plots['plot_number']) ?></a>
				</li>				
			<?php endif ?>			
		<?php endforeach ?>
	</ul>
<?php endif ?>

</td>
							<td>
<?php echo ucwords($value['person']['pers_gender']) ?>
							</td>
							<td>{{Form::delete('customers/'. $value['cust_id'], 'Delete')}}</td>
					</tr>						
					<?php endforeach ?>
					<?php else: ?>
					<tr>
						<td colspan="7"><h4>No Customer Available!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')