<?php

class Kin extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = ['fname','mname','lname','contacts','customer_id','ll_id'];
	public function landlord(){
		return $this->belongsTo('Landlord','ll_id','id');
	}
	public function customer(){
		return $this->belongsTo('Customer','customer_id','id');
	}
}