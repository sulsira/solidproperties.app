<?php

class Rentpayment extends \Eloquent {
	protected $table = 'rent_payments';
	protected $fillable = [
'paym_userID',
'paym_rentID',
'paym_houseID',
'paym_forMonths',
'paym_paidAmount',
'paym_balance',
'paym_currentBal',
'paym_date',
'paym_deleted',
'entered_months',
'owing',
'monthsfrom',
'monthsto',
'overpay',
'paym_remarks'
];

public function rent(){
	return $this->belongsTo('Rent','paym_rentID','rent_id');
}
}