<?php

class Rent extends \Eloquent {
	protected $primaryKey = 'rent_id';

	protected $fillable = [
		'rent_id',
		'rent_houseID',
		'rent_tenantID',
		'rent_monthlyFee',
		'rent_type',
		'rent_advance',
		'rent_firstmonthpaid',
		'rent_nextpaydate',
		'deleted',
		'rent_lastpaydate',
		'rent_balance'
	];
	public function tenant(){
		return $this->belongsTo('Tenant','rent_id','tent_rentID');
	}
	public function payments(){
		return $this->hasMany('Rentpayment','paym_rentID','rent_id');
	}
}