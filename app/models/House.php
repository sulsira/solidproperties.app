<?php

class House extends \Eloquent {
	protected $primaryKey = 'hous_id';
	protected $fillable = [
		'hous_numberOfrooms',
		'hous_number',
		'hous_description',
		'hous_id',
		'hous_compoundID',
		'hous_tenantID',
		'hous_advance',
		'hous_price',
		'hous_paymentStype',
		'hous_availability',
		'hous_status', # 0 - not occupied, 1 = mean occupied, 2 = pending to be empty, 3 = not for renting;
		'deleted'
	];

	public function compound(){
		return $this->belongsTo('Compound','hous_compoundID','comp_id');
	}
	public function scopeAvailable($query){
		return  $query->whereRaw('hous_tenantID = ?',[0])->get();
	}
	public function tenants(){
		return $this->hasMany('Tenant','tent_houseID','hous_id');
	}
}