<?php

class Transaction extends \Eloquent {
	protected $fillable = [
'trans_id',
'trans_custID',
'trans_payID',
'trans_currentBal',
'trans_dueBal',
'trans_totalBal',
'trans_status',
'trans_visible',
'trans_deleted'
	];

	public function customer(){
		return $this->belongsTo('Customer','cust_id','trans_custID');
	}
}