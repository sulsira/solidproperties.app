<?php namespace services\menus;

abstract class Menu{
	public $raw = array();
	public $incoming;
	public $menu = array();
	public $done_menu;
	public  $default = [
			[
			'name'=>'home',
			'visible'=> 0 ,
			'url'=>'/admin',
			'domains'=>'all',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['all']
						]
			 ],
			[
			'name'=>'land lords',
			'visible'=> 0 ,
			'url'=>'/land-lords',
			'domains'=>'users',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['all']
						]
			 ],
			[
			'name'=>'compounds',
			'visible'=> 0 ,
			'url'=>'/land-lords',
			'domains'=>'rent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['rent|admin|access']
						]
			 ],
			[
			'name'=>'houses',
			'visible'=> 0 ,
			'url'=>'/houses',
			'domains'=>'rent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['rent|admin|access']
						]
			 ],
			[
			'name'=>'notifications',
			'visible'=> 0 ,
			'url'=>'/notifications',
			'domains'=>'all',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['all|admin|access']
						]
			 ],
			[
			'name'=>'plots',
			'visible'=> 0 ,
			'url'=>'/plots',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['agent|admin|access']
						]
			 ],
			[
			'name'=>'agents',
			'visible'=> 0 ,
			'url'=>'/agents',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['agent|admin|access']
						]
			 ],
			[
			'name'=>'staff',
			'visible'=> 0 ,
			'url'=>'/staffs',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['admin|access']
						]
			 ],
			[
			'name'=>'customers',
			'visible'=> 0 ,
			'url'=>'/customers',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['agent|admin|access']
						]
			 ],
			[
			'name'=>'partners',
			'visible'=> 0 ,
			'url'=>'/partners',
			'domains'=>'agent',
			'type'=>'single',
			'security'=>[
						'level'=> 1 ,
						'views'=> ['agent|admin|access']
						]
			 ],
			[
			'name'=>'transactions',
			'visible'=> 0 ,
			'url'=>'/transactions',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 8 ,
						'views'=> ['admin|access']
						]
			 ],
			[
			'name'=>'Prospectives',
			'visible'=> 0 ,
			'url'=>'/prospectives',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 5 ,
						'views'=> ['agent|admin|access']
						]
			 ],
			[
			'name'=>'Tenants',
			'visible'=> 0 ,
			'url'=>'/tenants',
			'domains'=>'rent',
			'type'=>'single',
			'security'=>[
						'level'=> 5 ,
						'views'=> ['agent|admin|access']
						]
			 ],
			[
			'name'=>'Users',
			'visible'=> 0 ,
			'url'=>'/users',
			'domains'=>'admin',
			'type'=>'single',
			'security'=>[
						'level'=> 7 ,
						'views'=> ['admin|access']
						]
			 ],
			[
			'name'=>'--username--',
			'visible'=> 0 , 'url'=>'#','domains'=>'--self--','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 0 , 'url'=>'help','domains'=>'veda','type'=>'single','security'=>'8'],
				['name'=>'settings','visible'=> 0 , 'url'=>'admin/settings','domains'=>'veda','type'=>'single','security'=>'8'],
				['name'=>'logout','visible'=> 0 , 'url'=>'logout','domains'=>'veda','type'=>'single','security'=>'8']
			]]
		];
	// it would would try pick up what session you are on and your previledges
	// displays the pages you need to see by domain and clearance level
	// algorythm
		/**
		 * methods:
		 *        contruct incase a menu array is sent
		 *  /	 method( ) menu that return exactly what the view should see
		 *
		 * @return Response {}
		 */
		private function processMenu(){
			$session  = \Session::all();
			$menu = array();
			var_dump($session);
			if (empty($this->incoming)) {
				foreach ($this->default as $key => $value) {
					$menu[] = $value;
				}
			}else{
				foreach ($this->incoming as $key => $value) {
					$menu[] = $value;
				}
			}
			$this->done_menu = $menu;
		}

		public function getMenu(){
			$this->processMenu();
			return $this->done_menu;
		}
}
