<?php namespace services\validators;

class Landpayment extends Validate{
		public static $rules = [
		'amount_paid'=> 'required|numeric',
		'payment_plot'=> 'required',
		'payment_remark'=> 'max:200'
		// 'user_password'=> 'required|max:200|exists:users,password'
		// 'payment_code'=> 'required|max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}
